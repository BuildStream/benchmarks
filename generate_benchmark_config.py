#
#  Copyright (C) 2018 Codethink Limited
#  Copyright (C) 2018 Bloomberg Finance LP
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Lachlan Mackenzie <lachlan.mackenzie@codethink.co.uk>


import tempfile
import argparse
import os
import logging
from shutil import copyfile
import sys
import git
import yaml

from bst_benchmarks.config import DEFAULT_BASE_DOCKER_IMAGE, DEFAULT_BUILDSTREAM_REPO


# This function generates a benchmark configuration file that allows for
# multiple buildstream commits to be benchmarked individually.
#
# output_file - the full path for the generated benchmark configuration file
# list_of_shas - list of Buildstream commits that need to be processed
# docker_version - the docker version to be used in the configuration, set
#                  to latest, but might be more restricted in the future
# bs_branch - the Buildstream branch that is being considered, defaults to
#             master
# bs_path - path to the Buildstream repo (url or local directory)
# docker_path - path to the Docker Image to be used


def main():
   # Define all the defaults explicitly
   repo_path = 'https://gitlab.com/BuildStream/buildstream.git'
   bs_branch = 'master'
   shas_to_be_tested = []
   docker_tag = "30-latest"
   docker_image = 'registry.gitlab.com/buildstream/buildstream-docker-images/testsuite-fedora'
   debug = False

   def make_help(message):
      return message + " (Default: %(default)s)"

   # Resolve commandline arguments
   parser = argparse.ArgumentParser(description="Automatically set up test environments for a set "
                                    "of buildstream benchmarks and run them locally using docker.")
   parser.add_argument("-o", "--output_file",
                       help=make_help("The file to write benchmark config to"),
                       default="demanded.benchmark")
   parser.add_argument("-r", "--repo_path",
                       help=make_help("The repository from which to clone branches to compare "
                                      "against the defaults set in the test. Note that this can"
                                      " be a local path using file://"),
                       default=repo_path,
                       type=str)
   parser.add_argument("-b", "--bs_branch",
                       help=make_help("The branch to clone from the set repository"),
                       default=bs_branch,
                       type=str)
   parser.add_argument("-s", "--shas_to_be_tested",
                       help=make_help("SHAs to clone from the set repository"),
                       action='append')
   parser.add_argument("-d", "--docker_tag",
                       help=make_help("The tag to use for the buildstream image"),
                       default=docker_tag,
                       type=str)
   parser.add_argument("-p", "--docker_image",
                       help=make_help("The docker image to use"),
                       default=docker_image,
                       type=str)
   parser.add_argument("-g", "--debug",
                       help=make_help("Show debug messages"),
                       default=debug,
                       action='store_true')
   args = parser.parse_args()

   if bool(args.output_file):
      output_file = args.output_file

   if bool(args.repo_path):
      repo_path = args.repo_path

   if bool(args.bs_branch):
      bs_branch = args.bs_branch

   if bool(args.shas_to_be_tested):
      shas_to_be_tested = args.shas_to_be_tested

   if bool(args.docker_tag):
      docker_tag = args.docker_tag

   if bool(args.debug):
      debug = True
      logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
   else:
      logging.basicConfig(stream=sys.stderr, level=logging.INFO)

   commits = list()

   # Create a temporary directory for all work
   with tempfile.TemporaryDirectory(prefix='temp_staging_location') as temp_staging_area:
      # Get a reference to the requested repository; cloning if remote
      try:
         if os.path.exists(repo_path):
            logging.info("Repo path resolves locally: %s", repo_path)
            repo = git.Repo.init(repo_path, bare=False)
            # Checkout the user specific bs branch
            repo.git.checkout(bs_branch)
         else:
            logging.info("Repo path resolves remotely: %s", repo_path)
            repo = git.Repo.clone_from(repo_path, temp_staging_area)
            repo.git.checkout(bs_branch)
      except git.exc.GitError as err:  # pylint: disable=no-member
         logging.error("Unable to access git repository: %s", err)
         sys.exit(1)

      # Iterate the commits in the requested branch and add to list to be
      # processed if they are as per the command line selection. Keep a
      # list of all SHAs that have been found.
      shas_found = []
      #If no sha list provided assume head of bs branch.
      if not shas_to_be_tested:
         shas_to_be_tested.append(repo.head.commit.hexsha)

      try:
         for commit in repo.iter_commits():
            if commit.hexsha in shas_to_be_tested:
               commits.append(commit)
               shas_found.append(commit.hexsha)
      except git.exc.GitCommandError as err:  # pylint: disable=no-member
         logging.error("Could not find commits in repository '%s' for branch '%s':\n%s",
                       repo_path, bs_branch, err)
         sys.exit(1)

      # Check list of found SHAs against original list and flag any missing
      shas_not_found = [sha for sha in shas_to_be_tested if sha not in shas_found]
      if shas_not_found:
         logging.error("SHA(s) could not be found: %s", shas_not_found)
         sys.exit(1)

      # Create a temporary file reference for the benchmark versioning configuration
      # file that will be processed together with the selected benchmark test(s).
      output_tmp_file = os.path.join(temp_staging_area, 'output_file.benchmark')

      # Generate the bechmark versioning configuration file for selected parameters
      try:
         generate_benchmark_configuration(
            output_file=output_tmp_file,
            list_of_shas=commits,
            docker_version=docker_tag,
            bs_branch=bs_branch,
            bs_path=repo_path,
            docker_path=docker_image)
      # pylint: disable=broad-except
      except Exception as err:
         logging.error("Creating benchmarking configuration failed:\n%s", err)
         sys.exit(1)

      # Copy the resultant temporary file to the desired output
      copyfile(output_tmp_file, output_file)


def generate_benchmark_configuration(
      output_file="generated.benchmark",
      list_of_shas=None,
      docker_version="30-latest",
      bs_branch='master',
      bs_path=DEFAULT_BUILDSTREAM_REPO,
      docker_path=DEFAULT_BASE_DOCKER_IMAGE):

   if not list_of_shas:
      list_of_shas = []

   # Iterate through the list of shas and populate the stubbed entries
   # with sha data from the entry, then write each entry to the new file
   # entry
   with open(output_file, 'w') as yaml_file:

      version_default = {'version_defaults': {'base_docker_image': docker_path,
                                              'buildstream_repo': bs_path}}
      yaml.dump(version_default, yaml_file, default_flow_style=False)
      yaml_file.write('\n\n')

      configs = []
      logging.info("Generating configuration for %s Buildstream SHAs", str(len(list_of_shas)))
      for entry in list_of_shas:
         configs.append({'name': str(entry),
                         'base_docker_ref': docker_version,
                         'buildstream_ref': bs_branch,
                         'buildstream_commit': str(entry),
                         'buildstream_commit_date': str(entry.committed_date)})
      version = {'versions': configs}
      yaml.dump(version, yaml_file, default_flow_style=False)
      yaml_file.write('\n\n')


if __name__ == "__main__":
   main()
