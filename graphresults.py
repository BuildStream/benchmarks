#!/usr/bin/env python3

#
#  Copyright (C) 2018 Codethink Limited
#  Copyright (C) 2018 Bloomberg Finance LP
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Lachlan Mackenzie <lachlan.mackenzie@codethink.co.uk>

import json
import os
import logging
import argparse
import sys
import time
import datetime

import matplotlib.pyplot as plt

# Commandline executable that takes a directory containing json results files
# iterates through them and generates individual graph files of results based
# upon a given data set (denoted by test type name). Option is provided to
# provide a non-canonical results file (e.g a development branch in benchmark
# testing) as a comparable to canonical results. Non canonical results are not
# retained for future comparison. Option is provided to limit the number of
# datasets being graphed based upon the last measurement taken - typically
# this limits the number of individual commit references that need to be
# plotted - default is the last 24 hours.


def main():

   directory = '.'
   graphs_directory = directory
   files = list()
   max_plot_days = 1
   parser = argparse.ArgumentParser()
   parser.add_argument("-d", "--directory",
                       help="Directory containing files to be parsed",
                       type=str)
   parser.add_argument("-i", "--interim_results",
                       help="Path to result which is not on main branch",
                       type=str)
   parser.add_argument("-o", "--output_directory",
                       help="Output directory for created graphs",
                       type=str)
   parser.add_argument("-t", "--time_span",
                       help="Maximum number of days a specific commit result "
                            "should be plotted", type=int)
   parser.add_argument("-l", "--limit_results",
                       help="Specify what subset of test results should be "
                            "considered.", action='append')
   args = parser.parse_args()

   if bool(args.directory):
      if os.path.isdir(args.directory):
         for entry in os.scandir(args.directory):
            if entry.name.endswith(".json"):
               files.append(entry.path)
      else:
         logging.error("Specified directory does not exist: %s", args.directory)
         sys.exit(1)

   logging.debug("Parsing directory for graph data: %s", directory)

   if bool(args.interim_results):
      if os.path.isfile(args.interim_results):
         if args.interim_results.endswith(".json"):
            files.append(args.interim_results)
      else:
         logging.error("Interim result file does not exist: %s", args.interim_results)
         sys.exit(1)

   if bool(args.output_directory):
      if not bool(os.path.isdir(args.output_directory)):
         try:
            os.makedirs(args.output_directory)
            graphs_directory = args.output_directory
         except OSError:
            logging.error("Unable to create output directory: %s",
                          args.output_directory)
            sys.exit(1)
      else:
         if os.path.isdir(args.output_directory):
            graphs_directory = args.output_directory
         else:
            logging.error("Output directory does not exist: %s", args.output_directory)
            sys.exit(1)

   if bool(args.time_span):
      if args.time_span > 0:
         max_plot_days = args.time_span
      else:
         logging.error("Maximum number of days to retain must be greater than 0")
         sys.exit(1)

   graphdict = dict([])
   for entry in files:
      try:
         with open(entry, "r") as f:
            file = f.read()
            data = json.loads(file)
            try:
               measurement_time = data["end_timestamp"]
               version_ref = {}
               version_date = {}
               version_stale = {}
               version_name = {}
               v_index = ''

               for version in data["versions"]:
                  if "unique_ref" in version:
                     v_index = "unique_ref"
                  else:
                     v_index = "name"
                  version_ref[str(version[v_index])] = version["buildstream_commit"]
                  version_name[str(version[v_index])] = version["name"]

                  if 'buildstream_commit_date' in version:
                     if version["buildstream_commit_date"]:
                        version_date[str(version["buildstream_commit"])] = float(
                           version["buildstream_commit_date"])
                        if str(version[v_index]) == str(version["buildstream_commit"]):
                           now_time = datetime.datetime.now()
                           past_measurement = datetime.datetime.fromtimestamp(measurement_time)
                           if (
                                 now_time -
                                 past_measurement) > datetime.timedelta(
                                    days=max_plot_days):
                              version_stale[version[v_index]] = version["buildstream_ref"]
                     else:
                        version_date[str(version["buildstream_commit"])] = measurement_time
                  else:
                     version_date[str(version["buildstream_commit"])] = measurement_time

               for test in data["tests"]:
                  name = test["name"]
                  if bool(args.limit_results):
                     if name not in args.limit_results:
                        continue
                  for result in test["results"]:
                     version = result["version"]

                     if version in version_stale:
                        logging.info("Version marked as stale %s", version)
                        version = version_stale[version]

                     total_time = 0.0
                     total_max_rss_kb = 0.0
                     count = 0

                     # Check if measurements have been made
                     if "measurements" not in result:
                        logging.error("Measurement corruption in: %s", entry)
                        continue

                     bs_ref = None
                     bs_commit = None
                     # Iterate measurements and add
                     for measurement in result["measurements"]:
                        total_time = total_time + measurement["total-time"]
                        total_max_rss_kb = total_max_rss_kb + measurement["max-rss-kb"]
                        if ("bs-ref" in measurement) and ("bs-sha" in measurement):
                           if bs_ref is None:
                              bs_ref = measurement["bs-ref"]
                              bs_commit = measurement["bs-sha"]
                           else:
                              if measurement["bs-ref"] != bs_ref:
                                 logging.error(
                                    "Buildstream reference changed from %s to %s: ",
                                    bs_ref, measurement["bs-ref"])
                                 bs_ref = result["bs-ref"]
                              if measurement["bs-sha"] != bs_commit:
                                 logging.error(
                                    "Buildstream commit changed from %s to %s: ",
                                    bs_commit, measurement["bs-sha"])
                                 bs_commit = result["bs-sha"]

                        count += 1

                     if str(version) in version_ref:
                        commit = version_ref[str(version)]
                     else:
                        commit = version_ref[str(commit)]

                     if str(commit) in version_date:
                        commit_time = version_date[str(commit)]
                     else:
                        commit_time = version_date[str(version)]

                     # Calculate average
                     average_time = total_time / count
                     average_max_rss_kb = total_max_rss_kb / count

                     # Create a key based on version and name
                     key = {}
                     key['version'] = version_name[str(version)]
                     key['name'] = name
                     ice_key = frozenset(key.items())

                     # Create a value for the entry
                     value = [
                        commit,
                        measurement_time,
                        average_time,
                        average_max_rss_kb,
                        commit_time]

                     # Add the value to the accumulated values for a given key
                     if ice_key not in graphdict:
                        graphdict[ice_key] = []
                        graphdict[ice_key].append(value)
                     else:
                        graphdict[ice_key].append(value)

                     logging.debug("%s %s %s %s %s %s", version, name,
                                   commit, measurement_time, average_time,
                                   average_max_rss_kb)
            except ValueError as error:
               logging.error("Error during parse of %s: %s", file, error)
      except ValueError as error:
         logging.error("Failure to load %s as json file: %s", file, error)

   plt.ioff()

   for key, value in graphdict.items():
      dict_k = dict(key)
      graph_title = "Test Base Version: {} Test Name: {}".format(dict_k['version'], dict_k['name'])
      timestr = time.strftime("%Y%m%d-%H%M%S")
      file_name = dict_k['version'] + '_' + dict_k['name'] + '_' + timestr + '.png'

      average_time_name = "average_time_" + file_name
      average_time_name = average_time_name.replace(" ", "_")
      average_time_name = average_time_name.replace("\'", "")
      average_time_name_path = os.path.join(graphs_directory, average_time_name)

      average_kg_name = "average_kb_" + file_name
      average_kg_name = average_kg_name.replace(" ", "_")
      average_kg_name = average_kg_name.replace("\'", "")
      average_kg_name_path = os.path.join(graphs_directory, average_kg_name)

      list_a = list(value)
      list_a.sort(key=lambda x: x[1])
      times = list()
      average_times = list()
      average_kb = list()
      for data_set in list_a:
         times.append(datetime.datetime.fromtimestamp(data_set[4]))
         average_times.append(data_set[2])
         average_kb.append(data_set[3])

      plt.plot_date(times, average_times, 'bs')
      plt.title(graph_title, fontsize=10)
      plt.ylabel("Average Times")
      plt.xlabel("Date/Time")
      plt.xticks(rotation=45)
      plt.gcf().subplots_adjust(bottom=0.25)
      plt.gcf().autofmt_xdate()
      plt.savefig(average_time_name_path)
      plt.clf()

      plt.plot_date(times, average_kb, 'g^')
      plt.title(graph_title, fontsize=10)
      plt.ylabel("Average KB")
      plt.xlabel("Date/Time")
      plt.xticks(rotation=45)
      plt.gcf().subplots_adjust(bottom=0.25)
      plt.gcf().autofmt_xdate()
      plt.savefig(average_kg_name_path)
      plt.clf()


if __name__ == "__main__":
   main()
