# Configure benchmarking for one of the following run options and initiate:
# 1. Bot operation - branch and buildstream repository are passed via ci build variables and are
#                    then used to create a bespoke configuration file for benchmarking to be run
#                    against (as well as the configuration defined in the tests themselves and
#                    which are carried out by default).
# 2. Cron job follow buildstream - benchmarking that is carried out on a nightly basis for
#                    buildstream master for all commits prior to the last cron job run together
#                    with those configured in the test themselves.
# 3. Benchmarking default run - benchmarking that is carried out with no configuration (e.g.
#                    pushing a benchmarking development branch). The default benchmarking settings
#                    set in the tests are used for benchmarking.
# 4. Adhoc settings - in certain cases adhoc settings are required for various reasons, typical
#                     examples are - developmental testing of changes that might effect one of
#                     the modes above, but without effecting cached results (published) - runs to
#                     recover corruption of missing data sets during cron job runs.
configure_and_run_benchmark() {
  # Check if configuration has been passed in if not use default.
  if [ $# -ne 0 ]; then
    benchmark_conf="$1"
  else
    benchmark_conf="-c bst_benchmarks/default.benchmark"
  fi

  # If bot configuration and buildstream branch and repo have been set via ci variables
  if ! [ -z "${BS_BRANCH}" ]; then
    params=("-o bot-config.benchmark" "-b ${BS_BRANCH}" "${BS_REPO_PATH:+-r $BS_REPO_PATH}")
    # Generate configuration
    python3 generate_benchmark_config.py ${params[@]}
    # Run the benchmark test with the bot configuration
    ci_run "-c bot-config.benchmark ${benchmark_conf}"
  # If a follow of buildstream
  elif ! [ -z "${FOLLOW_BUILDSTREAM}" ]; then
    run_walk_ci "${benchmark_conf}"
  # Run as default.
  else
    ci_run "${benchmark_conf}"
  fi
}


# function constructs filtering parameters for graph results, to restrict
# generated graphs to those tests that relate to the current given job only.
# list_tests_in_results_file.py extracts the names of the relevant tests that associate
# with the current jobs results and use it to form the name to be used.
create_graph_config() {
  local tests=""
  local quote='"'
  while IFS= read -r line; do
      tests+="-l ${quote}${line}${quote} "
  done < <( ./list_tests_in_results_file.py -f "$1")
  echo "$tests"
}


# Script that implements requested benchmarking, configures and triggers the
# generating of graphs and web pages and places results in required ci cached
# directories depending on requested benchmarking purpose.
ci_run() {
  # Set job and time references
  local bm_branch="${BRANCH_REF}"
  local now=$(date +"%F")
  local nowt=$(date +"%T")
  local job_name="${JOB_NAME_REF}"

  # Set defaults for results and output paths
  local default_result_path="results_out"
  local cache_path="results_cache"
  local graph_cache_path="graph_cache"
  local pipeline_cache_path="pipeline_cache"
  # Set initial path for generated graphs
  local graph_path="$default_result_path/graph_set/"
  # Set results file names - time entries used to differentiate between multiple results
  # from different parts of the ci pipeline placed in the common shared directories.
  local results_file_name="results-$now-$nowt.json"
  local default_file_name="results.json"
  local digest_file_name="$job_name-digest.csv"
  # Set intermiate path for generated web pages.
  local web_page_output_path="$default_result_path/web_pages/$job_name"
  # Define a default for the number of days in which inidividual sha results should be
  # plotted individually before being folded into master results.
  local plot_days="1"

  # Remove any old previous cached web pages
  echo "Removing $web_page_output_path"
  rm -rf "$web_page_output_path"

  local benchmark_conf=""
  # Check if any specific configuration has been passed in and set local variable
  if [ $# -ne 0 ]; then
    benchmark_conf="$1"
  fi

  # Check if specific configuration has been set explicitly by ci variable and
  # over-ride local settings.
  if ! [ -z "${BENCHMARK_CONFIG}" ]; then
    benchmark_conf="${BENCHMARK_CONFIG}"
  fi

  # Check if the number of days worth of shas to plot has been overriden via ci
  # variable and change local reference as required.
  if ! [ -z "${DAYS_TO_PLOT}" ]; then
    plot_days="${DAYS_TO_PLOT}"
  fi

  # Unistall and re-install due to dependency issues with numpy and
  # path conflicts betweeen different numpy versions
  pip3 uninstall -y matplotlib
  pip3 uninstall -y numpy
  pip3 install matplotlib

  # If not a benchmarking operation using benchmarking master or a bespoke/debug of master
  # operation
  if [ "$bm_branch" != "master" ] && [ -z "${BESPOKE_WRITE}" ] && [ -z "${DEBUG_MASTER}" ]; then
   python3 -m bst_benchmarks $benchmark_conf -o $default_result_path/$default_file_name
   # Check if results file has been generated if not then exit
   if ! [ -e $default_result_path/$default_file_name ]; then
     exit 1
   fi
   # Generate digest file for results
   python3 digest_results.py -s $default_result_path/$default_file_name -o $default_result_path/$digest_file_name -j $job_name
   # Generate specific filename for graphing output file
   line=$( create_graph_config $default_result_path/$default_file_name )
   # Command line for generating the graph results for the output file
   com="python3 graphresults.py -d $cache_path/ -i $default_result_path/$default_file_name -t $plot_days -o $graph_path $line"
   # Evaluate the command for generating graphs
   eval $com
   # Display the result through ci logging.
   cat $default_result_path/$default_file_name
  else
   # If publishing results via ci pipeline build artifacts or a debug configuration
   if ! [ -z "${PUBLISH_RESULT}" ] || ! [ -z "${DEBUG_MASTER}" ]; then
    # Output file set to be deposited into pipeline cache for later possible collection at the
    # end of the pipeline.
    local ofile="$pipeline_cache_path/$results_file_name"
    # Run benchmarking given the requested configuration
    python3 -m bst_benchmarks $benchmark_conf -o $ofile
    # Check if output file has been created
    if ! [ -e $ofile ]; then
      exit 1
    fi
    # Generate digest file for results
    python3 digest_results.py -s $ofile -o $pipeline_cache_path/$digest_file_name -j $job_name
    # Generate specific filename for graphing output file
    line="$(create_graph_config "$ofile")"
    # Create command line for generating graph results
    com="python3 graphresults.py -d $cache_path/ -i $ofile -t $plot_days -o $graph_path $line"
    # Evaluate command line for generating graph results
    eval $com
   else
    # If not publishing results just carry out benchmarking and output results into default
    # job result directory.
    local ofile="$default_result_path/$default_file_name"
    python3 -m bst_benchmarks $benchmark_conf -o $ofile
    # Check if output file has been created and exit if not
    if ! [ -e $ofile ]; then
      exit 1
    fi
    # Generate digest file for results
    python3 digest_results.py -s $ofile -o $default_result_path/$digest_file_name -j $job_name
    # Generate specific filenme for graphing output file
    line="$(create_graph_config "$ofile")"
    # Create command line for generating graph results
    com="python3 graphresults.py -d $cache_path/ -i $default_result_path/$default_file_name -t $plot_days -o $graph_path $line"
    # Evaluate command line for generating graph results
    eval $com
   fi
  fi

  # Create directory structure for web page outputs
  echo "Creating $web_page_output_path"
  mkdir -p $web_page_output_path
  mkdir -p "$web_page_output_path/graph_set"
  mkdir -p "$web_page_output_path/public"

  # Copy graphs into the web page output path
  mv $graph_path "$web_page_output_path/"

  # Generate web pages using the generated graphs and the directory structure created
  python3 publishresults.py -d "$web_page_output_path/graph_set/" -o "$web_page_output_path/" -t "$default_result_path/web_pages/"

  # Remove the original graphs (the publishing creates its own structure incorporating these)
  rm -rf "$web_page_output_path/graph_set"

  # If either the publishing ci variable is set or a debug it set then copy the webpages into the
  # pipeline cache directory for potential use at the end of the pipeline.
  if ! [ -z "${PUBLISH_RESULT}" ] || ! [ -z "${DEBUG_MASTER}" ]; then
   cp --verbose -r "$web_page_output_path" "$pipeline_cache_path/public/"
  fi
}


# Script that creates a benchmarking configuration for multiple shas. If a bespoke configuration
# then all the commits between the start and end shas (provided via ci build variables) are used
# to create a configuration that will benchmark each. If not a bespoke configuration then the run
# token contained within the results cache directory (which also contains historical results) will
# be checked against the the current buildstream head sha and a configuration generated for all
# shas that have been added to buildstream master post the token configuration when compared against
# current master.
run_walk_ci() {
  local benchmark_conf=""
  # Check if a configuration has been set via ci build variables.
  if [ $# -ne 0 ]; then
    benchmark_conf="$1"
  fi
  # Evaluate the current buildstream head sha and export as environment variable
  eval "export BUILDSTREAM_HEAD=$(git ls-remote https://gitlab.com/BuildStream/buildstream.git HEAD | awk '{ print $1 }')"
  # echo result for ci logging confirmation
  echo $BUILDSTREAM_HEAD
  # Check if a bespoke configuration has been set
  if ! [ -z "${BESPOKE_CONFIG}" ]; then
    # Generate bespoke configuration using ci varaible provided start and end shas
    python3 generate_buildstream_config.py --bespoke_config --output_file bs_catchup.benchmark --bespoke_start_sha "${START_SHA}" --bespoke_end_sha "${END_SHA}"
  else
    # Generate configuration using last run token and the current buildstream head (as resolved via environment variable)
    # output file to required file.
    python3 generate_buildstream_config.py --output_file bs_catchup.benchmark --results_path 'results_cache/' --token_path 'results_cache/run_token.yml'
    # Copy generated file to pipeline cache for potential debug purposes.
    cp 'bs_catchup.benchmark' 'pipeline_cache/'
  fi
  # Create a concatination of configurations using passed in configuration and the generated configuration file
  benchmark_conf="$benchmark_conf -c bs_catchup.benchmark"
  # Run the benchmarking via the ci run script using the created configuration
  ci_run "$benchmark_conf"
  # If a bespoke configuration, debug or cron job following of buildstream copy updated run token
  # to pipeline cache for possible later update of the results cache entry (only done if all jobs
  # pass and publish variable is set).
  if [ -z "${BESPOKE_CONFIG}" ] || ! [ -z "${DEBUG_MASTER}" ] || ! [ -z "${FOLLOW_BUILDSTREAM}" ]; then
    # Check if run token has already been copied into pipeline cache
    if [ ! -f 'pipeline_cache/run_token.yml' ]; then
        echo "Copying run token to pipeline cache for update"
        # Copy published run token into pipeline cache for update
        cp 'results_cache/run_token.yml' 'pipeline_cache/'
    fi
    # Update copied run token against new results resolved in pipeline cache.
    # Note: This incremental approach allows each job stage to be checked incrementally.
    python3 update_run_token.py -t 'pipeline_cache/run_token.yml' -i 'pipeline_cache/'
  fi
}

# Cleanup prior to running tests:
# Docker: Removed prior images, containers and volumes
# Results files: Clean out any remnants of web pages from previous runs
prep_benchmark_ci_job () {
  docker image prune -a -f
  docker container prune -f
  docker volume prune -f
  rm -rf results_out/web_pages/${JOB_NAME_REF}/*
}
