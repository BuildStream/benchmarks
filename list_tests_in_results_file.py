#!/usr/bin/env python3

#
#  Copyright (C) 2019 Codethink Limited
#  Copyright (C) 2019 Bloomberg Finance LP
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Lachlan Mackenzie <lachlan.mackenzie@codethink.co.uk>

# Commandline executable that takes a file containing benchmarking results
# and gives back the tests that were run to generate the results.

import argparse
import logging
import os
import json
import sys


def main():
   results_file = "results.json"
   parser = argparse.ArgumentParser()
   parser.add_argument("-f", "--results_file",
                       help="File containing results to be checked",
                       type=str)
   args = parser.parse_args()

   if bool(args.results_file):
      if os.path.isfile(args.results_file):
         results_file = args.results_file
      else:
         logging.error("Specified file does not exist: %s", args.results_file)
         sys.exit(1)

   with open(results_file, "r") as f:
      file = f.read()
      data = json.loads(file)
      tests = []
      try:
         for test in data["tests"]:
            tests.append(test["name"])
      except ValueError:
         logging.error("Unable to resolve tests in: %s", results_file)

   results = '\n'.join(tests)
   print(results)


if __name__ == "__main__":
   main()
