#
#  Copyright (C) 2018 Codethink Limited
#  Copyright (C) 2018 Bloomberg Finance LP
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Lachlan Mackenzie <lachlan.mackenzie@codethink.co.uk>

import os
import logging
import datetime
import tempfile
import argparse
import shutil
import ntpath
import operator
import sys

import git
import yaml
from dateutil.parser import parse

# A run token encapsulates data that describes the dynamics of the last
# benchmarking testing that was carried out and retained. This data is
# used to determine which SHAs need to be processed (i.e. the buildstream
# commits that were made since last time) and confirm consistency between
# buildstream and benchmarking branches/commits. A check is also made as
# to whether the expected last result that was written is still present
# and has not been superseeded by a failed run (warning at the minute).


def main():
   token_file = 'run_token.yml'
   results_files = 'results_cache/'
   repo_path = 'https://gitlab.com/BuildStream/buildstream.git'
   correct = False
   bs_branch = 'master'

   parser = argparse.ArgumentParser()
   parser.add_argument("-t", "--token_file",
                       help="The token file that is to be updated.",
                       type=str)
   parser.add_argument("-i", "--input_path",
                       help="Path for generated results",
                       type=str)
   parser.add_argument("-r", "--repo_path",
                       help="Repo path",
                       type=str)
   parser.add_argument("-c", "--correct",
                       help="Try to correct token file if corrupt",
                       action='store_true')
   parser.add_argument("-b", "--bs_branch",
                       help="Buildstream branch.",
                       type=str)

   args = parser.parse_args()

   if bool(args.token_file):
      token_file = args.token_file

   if bool(args.input_path):
      results_files = args.input_path

   if bool(args.repo_path):
      repo_path = args.repo_path

   if bool(args.correct):
      correct = True

   if bool(args.bs_branch):
      bs_branch = args.bs_branch

   token = process_token_file(token_file)

   # Check if results exist and get the last generated one
   if os.path.isdir(results_files):
      file_times = {}
      for file in os.listdir(results_files):
         if file.endswith('.json'):
            file_times[os.path.join(results_files, file)] = datetime.datetime.strptime(
               os.path.basename(file), 'results-%Y-%m-%d-%H:%M:%S.json')
      if file_times:
         latest_file = max(file_times.items(), key=operator.itemgetter(1))[0]
         archive_result = ntpath.basename(latest_file)
      else:
         logging.info('No results files found from: %s', results_files)
         archive_result = ''
   else:
      logging.error('Results path does not exist, nothing to update: %s', results_files)
      os.sys.exit(1)

   temp_staging_area = tempfile.mkdtemp(prefix='temp_staging_location')
   # Clone buildstream repo to temporary staging area
   try:
      with git.Repo.clone_from(repo_path, temp_staging_area) as bs_repo:
         if not verify_token_data(token, archive_result, results_files, bs_repo.working_dir):
            if correct:
               logging.error('Token file fails verification, creating new')
               bench_mark_repo = git.Repo(search_parent_directories=True)
               create_default_token_file(
                  bs_repo.working_dir,
                  token_file,
                  bs_branch,
                  archive_result,
                  bench_mark_repo)
            else:
               logging.error('Token file fails verification.')
               os.sys.exit(1)
   except git.exc.GitCommandError as err:  # pylint: disable=no-member
      logging.error("Unable to clone Buildstream Repo: path: %s, error: %s", repo_path, err)
      os.sys.exit(1)
   finally:
      shutil.rmtree(temp_staging_area)


# Function to parse a run token file and return the contents.

def process_token_file(file_path):
   try:
      with open(file_path, "r") as config:
         try:
            data_loaded = yaml.load(config)
            time = data_loaded['build']['time']
            buildstream_sha = data_loaded['build']['bs_sha']
            buildstream_branch = data_loaded['build']['bs_branch']
            benchmark_sha = data_loaded['build']['bm_sha']
            benchmark_branch = data_loaded['build']['bm_branch']
            last_result = data_loaded['build']['file']
         except yaml.YAMLError as y_err:
            logging.error("Unable to parse process token file: %s", y_err)
            raise
   except OSError as err:
      logging.error("Unable to access process token file: %s", err)
      raise err

   return {'build': {'time': time, 'bs_sha': buildstream_sha, 'bs_branch': buildstream_branch,
                     'bm_sha': benchmark_sha, 'bm_branch': benchmark_branch, 'result': last_result}}


# Verify the token data against a given configuration (i.e. the configuration for the
# current run - return false if there is an inconsistency with the possibility of having
# to regenerate a tenable configuration.

def verify_token_data(token_data, last_result, results_path, repo_path):

   # Check time parses
   try:
      parse_time = parse(token_data['build']['time'])
   except ValueError as v_error:
      logging.error("Time from token file does not resolve: %s", v_error)
      return False

   # Check time makes sense
   now = datetime.datetime.now()
   if now < parse_time:
      logging.error("Time from token is later than current resolved time: %s %s", now, parse_time)
      return False

   # Check buildstream_sha is still valid
   # assumes repo has already been cloned elsewhere
   try:
      if os.path.exists(repo_path):
         # Init repo from repo path
         repo = git.Repo.init(repo_path, bare=False)
         # Check buildstream branch is valid
         if not repo.git.rev_parse('--verify', token_data['build']['bs_branch']):
            logging.error('Branch not in repo: %s', token_data['build']['bs_branch'])
            return False
         # Check sha is still valid
         if token_data['build']['bs_sha'] not in [
               commit.hexsha for commit in repo.iter_commits(
                  token_data['build']['bs_branch'])]:
            logging.error('SHA of last commit not present in current repo branch')
            return False
      else:
         logging.error('Git Repo not valid')
         return False
   except git.exc.GitError as err:  # pylint: disable=no-member
      logging.error("Unable to access git repository: %s", err)
      return False
   # TODO: Sort out what this error here actually means
   except Exception as g_ex:   # pylint: disable=broad-except
      logging.error("Exception when attempting to validate buildstream details: %s", g_ex)
      return False

   # Check if previous bench mark sha is valid in the context of the current repo
   try:
      # Init repo based upon parent directory
      bench_mark_repo = git.Repo(search_parent_directories=True)
      # Check benchmark branch is valid
      if not bench_mark_repo.git.rev_parse('--verify', token_data['build']['bm_branch']):
         logging.error('Branch not in repo: %s', token_data['build']['bs_branch'])
         return False
      if token_data['build']['bm_sha'] not in [
            commit.hexsha for commit in bench_mark_repo.iter_commits(
               token_data['build']['bm_branch'])]:
         error_msg = 'SHA of previous benchmarking Head not in repo branch: %'
         logging.error(error_msg, token_data['build']['bm_sha'])
         return False
   except git.exc.GitError as err:  # pylint: disable=no-member
      logging.error("Unable to verify benchmark sha: %s", err)
      return False

   # Check if the last results file exists - this could become a check of the larger set
   # of results files.
   if last_result:
      last_result_recorded = token_data['build']['result']
      last_result_path = os.path.join(results_path, token_data['build']['result'])
      if last_result == last_result_recorded:
         return True
      if not os.path.exists(last_result_path):
         logging.error(
            'The last results file from the previous run does not exist: %s',
            last_result)
         return False
      if last_result_recorded != last_result:
         logging.error(
            'The last results file from the previous run does not match the expected last result. '
            'expected: %s vs resolved %s',
            last_result_recorded,
            last_result)
         return False

   return True

# Generate a token file given certain parameters, used if no token file is present
# or the token file that is currently in place needs replacing.


def generate_token_file(file_path, buildstream_sha, buildstream_branch, last_result):

   # Init repo based upon parent directory
   bench_mark_repo = git.Repo(search_parent_directories=True)

   # Check if current benchmark branch is detached
   if bench_mark_repo.head.is_detached:
      _bm_branch = bench_mark_repo.head.name
   else:
      _bm_branch = bench_mark_repo.active_branch


   # Write configuration file for token file
   data = dict(
      build=dict(
         time=str(datetime.datetime.now()),
         bs_sha=str(buildstream_sha),
         bs_branch=str(buildstream_branch),
         bm_sha=str(bench_mark_repo.head.object.hexsha),
         bm_branch=str(_bm_branch),
         file=str(last_result))
   )
   try:
      with open(file_path, 'w') as outfile:
         yaml.dump(data, outfile, default_flow_style=False)
   except BaseException:
      logging.error("Unable to write configuration file: %s", sys.exc_info()[0])
      raise


# Function to generate a default token file if the provided one doesn't exist
# or is not consistent with current configuration
#
# repo - path to local buildstream repo (so not duplicating the clone)
# run_token_path - path to the token file
# bs_branch - the buildstream branch under consideration
# archive_result - path to the most recent benchmark result
# bench_mark_repo - path to local benchmark repo (no duplication)

def create_default_token_file(repo, run_token_path, bs_branch, _archive_result, _bench_mark_repo):
   logging.error("Unable to access token file attempting to process all from master: ")
   commits = list()
   thresholdtime = repo.head.object.committed_date - (60 * 60 * 24)
   for commit in repo.iter_commits(bs_branch):
      if commit.committed_date >= thresholdtime:
         commits.append(commit)
   # Try to generate token file
   generate_token_file(run_token_path, str(commits[0]), bs_branch, '')


if __name__ == "__main__":
   main()
