#!/bin/bash

# Check the integrity of the run token, correct if erroneous on master branch run

if [ "$BRANCH" != "master" ]; then
  python3 token_file_processing.py -t 'results_cache/run_token.yml' -i 'results_cache/' -r 'https://gitlab.com/BuildStream/buildstream.git' -b 'master'
else
  python3 token_file_processing.py -t 'results_cache/run_token.yml' -i 'results_cache/' -r 'https://gitlab.com/BuildStream/buildstream.git' -b 'master' -c
fi

status=$?

if test $status -eq 0
then
  echo "Valid token file resolved"
else
  echo "Token file unresolved"
  exit 1
fi
