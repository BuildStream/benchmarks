#!/bin/bash


docker image prune -a -f
docker container prune -f
docker volume prune -f
rm -rf results_out/web_pages/${JOB_NAME_REF}/*
