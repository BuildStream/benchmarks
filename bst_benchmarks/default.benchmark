# Default configuration for BuildStream benchmarking tool.

# List of versions to test.
#
# Each version in tested in a separate Docker container which ensures that each
# version can have the correct dependency set available, and we can track
# exactly what that dependency set is.
version_defaults:
  base_docker_image: registry.gitlab.com/buildstream/buildstream-docker-images/testsuite-fedora
  buildstream_repo: https://gitlab.com/BuildStream/BuildStream

versions:
- name: stable
  base_docker_ref: 30-latest
  buildstream_ref: bst-1.2

- name: master
  base_docker_ref: 30-latest
  buildstream_ref: master


test_defaults:
  measurements_file: /root/measurements.json
  repeats: 3
  log_file: /root/log.txt

tests:
- name: Startup time
  script: |
    bst_measure -o '/root/measurements.json' bst --help

- name: Build of Baserock stage1-binutils for x86_64
  description: |
    Build a real BuildStream project and measure total build time.

    We are using the Baserock "reference systems" project here mostly because
    it can still be built with BuildStream 1.0.0.
  mounts:
  - volume: baserock-source
    path: /mnt/baserock-source
  script: |
    git clone https://gitlab.com/BuildStream/bst-plugins-experimental/
    cd bst-plugins-experimental
    git checkout 6cd4066fc192258b853b52f13c04866022953b96
    pip3 install --user .
    cd ..

    if [ ! -d /mnt/baserock-source/definitions ]
    then
       echo "baserock source does not exist"
       exit 1
    fi

    git clone https://gitlab.com/baserock/definitions
    cd definitions
    #TODO - The rationale behind using this historically based branch needs revisiting
    git checkout e9a44856d967b9c74029be256b89f7b7b9c2f035 # branch lachlanmackenzie/sam-bst-1.0.0-recreate-modify-for-plugin-changes

    # Use pre-created source cache to avoid measuring the network fetch time.
    mkdir -p ~/.config
    cp /root/usr_config_bst.yml ~/.config/buildstream.conf
    echo "sourcedir: /mnt/baserock-source" >> ~/.config/buildstream.conf

    # Disable the artifact cache to ensure a full build.
    sed -e '/artifacts:/,/^$/ d' -i project.conf

    # TODO - temporary fix for missing dependencies in bst-plugins-experimental
    pip3 install pycairo PyGObject

    bst_measure \
        -o /root/measurements.json \
        -l /root/log.txt \
        bst --log-file='/root/log.txt' build gnu-toolchain/stage1-binutils.bst

    # Or, for a more complete test: bst build systems/minimal-system-image-x86_64.bst
  repeats: 3

# This kind of test will be implemented once the project generation tool exists.
#
#  - name: Benchmark 1
#    generate-project:
#      elements: [100, 1000, 10000]
#    only-measure-elements:
#    - first.bst
#    - last.bst

volumes:
- name: baserock-source
  description: |
    Volume holding all sources needed for the Baserock components that we build.
  prepare:
    version: master
    repo: https://gitlab.com/BuildStream/BuildStream
    path: /mnt/baserock-source
    script: |
      git clone https://gitlab.com/BuildStream/bst-plugins-experimental/
      cd bst-plugins-experimental
      git checkout 6cd4066fc192258b853b52f13c04866022953b96
      pip3 install --user .
      cd ..

      mkdir -p ~/.config
      echo "sourcedir: /mnt/baserock-source" > ~/.config/buildstream.conf

      git clone https://gitlab.com/baserock/definitions /mnt/baserock-source/definitions
      cd /mnt/baserock-source/definitions
      #TODO - The rationale behind using this historically based branch needs revisiting
      git checkout e9a44856d967b9c74029be256b89f7b7b9c2f035 # branch lachlanmackenzie/sam-bst-1.0.0-recreate-modify-for-plugin-changes

      # TODO - temporary fix for missing dependencies in bst-plugins-experimental
      pip3 install pycairo PyGObject

      bst source fetch gnu-toolchain/stage1-binutils.bst

      # Or, for a more complete test: bst fetch systems/minimal-system-image-x86_64.bst
