# Debian configuration for BuildStream benchmarking tool.

# List of versions to test.
#
# Each version in tested in a separate Docker container which ensures that each
# version can have the correct dependency set available, and we can track
# exactly what that dependency set is.

version_defaults:
  base_docker_image: registry.gitlab.com/buildstream/buildstream-docker-images/testsuite-fedora
  buildstream_repo: https://gitlab.com/BuildStream/BuildStream

versions:
- name: master
  base_docker_ref: 30-latest
  buildstream_ref: master


test_defaults:
  measurements_file: /root/measurements.json
  repeats: 3
  log_file: /root/log.txt

tests:
- name: Show of Debian like project for x86_64
  description: |
    Test implements a bst show of Debian like skeleton project and measures total
    time. The project represents all the binaries and their runtime dependencies
    in Debian stretch.

  script: |
    git clone https://gitlab.com/jennis/debian-stretch-binaries-bst.git
    cd debian-stretch-binaries-bst
    git checkout 9d2f0ff8eef75b2ff3c9ae806e8b031e038e36f4

    # Added in case yaml_cache is present
    rm -rf .bst/

    /usr/bin/bst_measure -o '/root/measurements.json' -l /root/log.txt \
      bst --log-file='/root/log.txt' --config='/root/usr_config_bst.yml' show debian-stack.bst

  repeats: 3
