#
#  Copyright (C) 2018 Codethink Limited
#  Copyright (C) 2018 Bloomberg Finance LP
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Sam Thursfield <sam.thursfield@codethink.co.uk>
#        Jim MacArthur  <jim.macarthur@codethink.code.uk>
#        Lachlan Mackenzie <lachlan.mackenzie@codethink.co.uk>

# buildstream benchmark script


import datetime
import io
import json
import logging
import os
import sys
import tarfile
import tempfile
import subprocess

import git
import docker
import requests

from . import config
from . import hostanalysis


DEFAULT_CONFIG_FILE = os.path.join(os.path.dirname(__file__), 'default.benchmark')

# We use Docker image labels to identify the contents of our prepared images,
# enabling reuse. The documentation says "Authors of third-party tools should
# prefix each label key with the reverse DNS notation of a domain they own, such
# as com.example.some-label.".
#
# See: https://docs.docker.com/config/labels-custom-metadata/
DOCKER_LABEL_NAMESPACE = 'org.gnome.buildstream.benchmarks'

# This is how we name the Docker images we create.
DOCKER_REPOSITORY = 'bst_benchmarks'

# This is not a constant, but a global variable that keeps track of
# which containers we have created.
# TODO: Make this more robust by backing it with a file
container_set = []   # pylint: disable=invalid-name


class BstVersion():
   """Represents a version of BuildStream being tested.

   Args:
       spec (config.BstVersionSpec): Version specification
       image (docker.models.images.Image): Image to be used for running tests for this version
       base_docker_digest (str): Exact sha256 digest of the base Docker image
                                 (from which 'image' was built).
       buildstream_commit (str): Exact sha1 hash of the BuildStream commit.
       bst_version_string (str): Output of `bst --version` inside the container.
       buildstream_commit_date (str): Date/time of BuildStream commit.
       unique_re (str): Unique reference that describes the test instance (repo, test, ref/commit).
   """

   def __init__(
         self,
         spec,
         image,
         base_docker_digest,
         buildstream_commit,
         bst_version_string,
         buildstream_commit_date,
         unique_ref):
      self.spec = spec
      self.image = image
      self.base_docker_digest = base_docker_digest
      self.buildstream_commit = buildstream_commit
      self.bst_version_string = bst_version_string
      self.buildstream_commit_date = buildstream_commit_date
      self.unique_ref = unique_ref

   def describe(self):
      return {
         'name': self.spec.name,
         'base_docker_image': self.spec.base_docker_image,
         'base_docker_ref': self.spec.base_docker_ref,
         'base_docker_digest': self.base_docker_digest,
         'buildstream_repo': self.spec.buildstream_repo,
         'buildstream_ref': self.spec.buildstream_ref,
         'buildstream_commit': self.buildstream_commit,
         'buildstream_commit_date': self.buildstream_commit_date,
         'bst_version_string': self.bst_version_string,
         'unique_ref': self.unique_ref,
      }


class TestResult():
   """Results of running a specific test against some versions of BuildStream.

   Args:
       spec (config.TestSpec): Test specification
   """

   def __init__(self, spec):
      self.spec = spec

      self.results_for_version = dict()

   def record_result_for_version(
         self,
         version,
         average_measurements,
         returncode,
         output,
         exception):
      """Record results for this test against a specific version of BuildStream.

      Args:
          version (BstVersion): Version of BuildStream that was tested.
          average_measurements (dict): Measurements returned by the test script, or None
          returncode (int): Exit code of the test script, or None if it didn't run
          output (str): Output from the test script, or None if it didn't run
          exception (Exception): Exception if something broke internally, or None
      """
      result = {
         'version': version.spec.unique_ref,
      }

      if exception:
         result['exception'] = str(exception)

         # For successful runs we don't share the output to avoid noise, but
         # if anything failed it's useful to see what.
         if output:
            result['output'] = str(output)

      if returncode:
         result['returncode'] = returncode

      if average_measurements:
         result['measurements'] = average_measurements
         result['repeats'] = self.spec.repeats

      self.results_for_version[version.spec.unique_ref] = result

   def describe(self):
      return {
         'name': self.spec.name,
         'results': list(self.results_for_version.values())
      }


def get_image_label(image, label_name):
   return image.labels['{}.{}'.format(DOCKER_LABEL_NAMESPACE, label_name)]


def compare_image_label(image, label_name, expected_value):
   value = get_image_label(image, label_name)

   if value != expected_value:
      logging.debug(
         "Label %s has value %s, wanted value %s",
         label_name, value, expected_value)
      return False

   logging.debug("Label %s matches expected value %s", label_name, expected_value)
   return True


# Looks for an existing Docker image corresponding to 'version_spec'.
#
# The image metadata is checked to ensure that it matches the specified
# version. However we do not check for updates to the base Docker image or the
# commit of buildstream.git.
def reuse_prepared_version(docker_client, version_spec):
   image_name = '{}:{}'.format(DOCKER_REPOSITORY, version_spec.name)

   try:
      image = docker_client.images.get(image_name)
      logging.info("Found existing image named %s.", image_name)

      if not all([compare_image_label(image, 'base-docker-image', version_spec.base_docker_image),
                  compare_image_label(image, 'base-docker-ref', version_spec.base_docker_ref),
                  compare_image_label(image, 'buildstream-repo', version_spec.buildstream_repo),
                  compare_image_label(image, 'buildstream-ref', version_spec.buildstream_ref)]):
         logging.info("Image does not match the version spec, not reusing the image.")
         return None

      base_docker_digest = get_image_label(image, 'base-docker-digest')
      buildstream_commit = get_image_label(image, 'buildstream-commit')
      bst_version_string = get_image_label(image, 'bst-version-string')
      buildstream_commit_date = version_spec.buildstream_commit_date
      unique_ref = version_spec.unique_ref

      return BstVersion(
         version_spec,
         image,
         base_docker_digest,
         buildstream_commit,
         bst_version_string,
         buildstream_commit_date,
         unique_ref)

   except docker.errors.ImageNotFound:
      logging.info("Did not find existing image named %s", image_name)
      return None


def parse_output_var(line, prefix):
   if not line.startswith(prefix + ': '):
      raise RuntimeError("Line didn't start with expected prefix {}: {}".format(prefix, line))

   return line.split(': ', 1)[1]


# Creates a Docker image that can be used to test a specific version of
# BuildStream, as described by 'version_spec'.
#
# If 'reuse_images' is set to True, existing images with the same name will be
# considered for reuse. Docker labels are used to check whether an image matches
# the given spec.
#
# TODO: Make _reuse_images actually do something
def prepare_version(docker_client, version_spec, bs_repo, _reuse_images=False):
   logging.info("Preparing image '%s:%s'", DOCKER_REPOSITORY, version_spec.name)

   # The base image may have a symbolic name, so check that we have the latest
   # version.
   logging.debug(
      "Pulling base Docker image %s:%s",
      version_spec.base_docker_image,
      version_spec.base_docker_ref)
   docker_client.images.pull(version_spec.base_docker_image, version_spec.base_docker_ref)

   base_docker_image_with_version = '{}:{}'.format(
      version_spec.base_docker_image, version_spec.base_docker_ref)

   # Buildstream repo
   repo_dir = bs_repo[version_spec.buildstream_repo].git.rev_parse("--show-toplevel")
   bs_repo[version_spec.buildstream_repo].git.checkout(version_spec.buildstream_commit)
   mount_point = '/mnt/bs_repo'

   # The digest identifies the *exact* Docker image that we are using as a base.
   base_docker_image = docker_client.images.get(base_docker_image_with_version)
   base_docker_digest = base_docker_image.id

   # Start a container from the base image and run the setup script.
   #
   # Due to Docker limitations, we run setup as a single shell script. Luckily
   # we are only interpolating URLs and Git refs into this which should not
   # contain spaces or other weird characters.
   commands = [
      'set -e',
      'cd {}'.format(mount_point),
      'git clone --no-hardlinks . ~/buildstream --branch {}'.format(version_spec.buildstream_ref),
      'cd ~'
   ]

   if version_spec.buildstream_commit:
      commands += ['cd ~/buildstream',
                   'git checkout {}'.format(version_spec.buildstream_commit),
                   'cd ~',
                   ]

   commands += ['pip3 install --user ./buildstream',
                'echo \'export PATH="$PATH:$HOME/.local/bin"\' > "$HOME/.bash_profile"',
                'echo \'export BST_TEST_SUITE=SET\' >> "$HOME/.bash_profile"',
                # pylint: disable=line-too-long
                'echo \"export BUILDSTREAM_COMMIT=$(git -C ./buildstream rev-parse HEAD)\" >> "$HOME/.bash_profile"',
                # pylint: disable=line-too-long
                'echo \"export BUILDSTREAM_REF={}" >> "$HOME/.bash_profile"'.format(version_spec.buildstream_ref),
                'cp /mnt/docker/bst_benchmarks/bst_measure "/usr/bin/"',
                'cp /mnt/docker/bst_benchmarks/usr_config_bst.yml "$HOME/"',
                'source "$HOME/.bash_profile"',
                'echo "buildstream_commit: $BUILDSTREAM_COMMIT"',
                'echo "bst_version_string: $(bst --version)"']

   script = '\n'.join(commands)

   container = docker_client.containers.run(
      base_docker_image_with_version,
      command=[
         '/bin/bash',
         '-c',
         script],
      detach=True,
      volumes={
         os.getcwd(): {
            'bind': '/mnt/docker/',
            'mode': 'rw'},
         repo_dir: {
            'bind': mount_point,
            'mode': 'rw'}})

   try:
      response = container.wait()
      container_set.append(container.short_id)
      exit_code = response['StatusCode']
      if exit_code != 0:
         error = container.logs(stdout=True, stderr=True).decode('unicode-escape')
         logging.error("Container setup script exited with code %s. Error output:\n%s"
                       , exit_code, error)
         raise RuntimeError("Container setup script exited with code {}. Error output:\n{}"
                            .format(exit_code, error))

      output = container.logs(stdout=True).decode('unicode-escape')
      logging.debug("Output from setup script: %s", output)

      lines = output.splitlines()

      buildstream_commit = parse_output_var(lines[-2], 'buildstream_commit')
      bst_version_string = parse_output_var(lines[-1], 'bst_version_string')
      buildstream_commit_date = version_spec.buildstream_commit_date

      label_changes = '\n'.join([
         'LABEL {}.base-docker-image="{}"'
         .format(DOCKER_LABEL_NAMESPACE, version_spec.base_docker_image),
         'LABEL {}.base-docker-ref="{}"'
         .format(DOCKER_LABEL_NAMESPACE, version_spec.base_docker_ref),
         'LABEL {}.base-docker-digest="{}"'
         .format(DOCKER_LABEL_NAMESPACE, base_docker_digest),
         'LABEL {}.buildstream-repo="{}"'
         .format(DOCKER_LABEL_NAMESPACE, version_spec.buildstream_repo),
         'LABEL {}.buildstream-ref="{}"'
         .format(DOCKER_LABEL_NAMESPACE, version_spec.buildstream_ref),
         'LABEL {}.buildstream-commit="{}"'
         .format(DOCKER_LABEL_NAMESPACE, buildstream_commit),
         'LABEL {}.bst-version-string="{}"'
         .format(DOCKER_LABEL_NAMESPACE, bst_version_string),
      ])

      image = container.commit(
         repository=DOCKER_REPOSITORY,
         tag=version_spec.unique_ref,
         changes=label_changes)
   except BaseException as err:
      logging.error("Container setup failed with Error output:\n%s", err)
      container.remove()
      raise

   return BstVersion(
      version_spec,
      image,
      base_docker_digest,
      buildstream_commit,
      bst_version_string,
      buildstream_commit_date,
      version_spec.unique_ref)


# Prepares a Docker volume that can be mounted during test runs.
def prepare_volume(docker_client, volume_spec, versions_dict):
   logging.info("Preparing volume '%s'", volume_spec.prepare.version)

   try:
      unique_version_name = volume_spec.prepare.version + '.' + \
          volume_spec.prepare.repo.replace(':', '.').replace('/', '.').lower()
      version = versions_dict[unique_version_name]
      if not version:
         logging.error("Buildstream version used to create volume corrupt"
                       "unable to create volume %s", volume_spec.prepare.version)
         raise RuntimeError(
            "Volume requires corrupted version: '{}'".format(
               volume_spec.prepare.version))
   except KeyError:
      raise RuntimeError(
         "Volume requires unknown version: '{}'".format(
            volume_spec.prepare.version))

   volume = docker_client.volumes.create(name=volume_spec.prepare.version)

   scripts = 'source "$HOME/.bash_profile"\n' + volume_spec.prepare.script

   container = docker_client.containers.run(
      version.image, command=[
         '/bin/bash', '-c', scripts],
      detach=True,
      mounts=[
         docker.types.Mount(target=volume_spec.prepare.path, source=volume.id)
      ])

   try:
      response = container.wait()
      exit_code = response['StatusCode']
      if exit_code != 0:
         error = container.logs(stdout=True, stderr=True).decode('unicode-escape')
         raise RuntimeError("Volume setup script exited with code {}. Error output:\n{}"
                            .format(exit_code, error))

      output = container.logs(stdout=True).decode('unicode-escape')
      logging.debug("Output from setup script: %s", output)
   finally:
      container.remove()

   return volume


# Read the measurements file from inside the container that has run a test.
def read_measurements(container, path):
   logging.debug("Reading measurements file from path: %s", path)

   with tempfile.NamedTemporaryFile('wb') as tar:
      stream, _stat = container.get_archive(path)

      # This is an iterator since docker-py 3.0.0, so we can't stream
      # it directly to tarfile.open() without writing a custom adapter.
      for chunk in stream:
         tar.write(chunk)
      tar.flush()

      # The good thing is we can stream the file we want back out; this
      # wouldn't be possible if we passed a stream to tarfile.open() because
      # the extractfile() method needs to be able to seek backwards.
      with tarfile.open(tar.name, mode='r') as tar:
         data = tar.extractfile(os.path.basename(path))
         text = io.TextIOWrapper(data, encoding='utf-8')
         measurements = json.load(text)

   return measurements


# Runs one test against one version of BuildStream, with one or more repeats.
#
# The return value is a tuple of:
#
#   (measurements, returncode, error output, exception)
#
# If the test returns a non-zero exit code or causes an exception then this
# function will return immediately, even if it was meant to repeat the test more
# times.
def run_benchmark(docker_client, version, test_spec, volumes_dict):
   logging.info("Running test '%s' on version '%s'. Repeats: %s",
                test_spec.name, version.spec.name, test_spec.repeats)

   if not version:
      logging.error("Unable to test required version, skipping test %s on version %s",
                    test_spec.name, version.spec.name)
      raise RuntimeError("Unable to test required version, skipping test {} on version {}"
                         .format(test_spec.name, version.spec.name))

   all_measurements = []

   for i in range(0, test_spec.repeats):
      logging.debug("Starting container from %s", version.image)

      mounts = []
      for mount in test_spec.mounts:
         volume = volumes_dict[mount.volume]
         if volume is None:
            logging.error("Unable to mount required volume, skipping test %s on version %s",
                          test_spec.name, version.spec.name)
            raise RuntimeError("Unable to mount required volume, skipping test {} on version {}"
                               .format(test_spec.name, version.spec.name))
         mounts.append(
            docker.types.Mount(target=mount.path, source=volume.id)
         )

      scripts = 'source "$HOME/.bash_profile"\n' + test_spec.script

      container = docker_client.containers.run(
         version.image,
         command=['/bin/bash', '-c', scripts],
         detach=True,
         devices='/dev/fuse:/dev/fuse:rwm',
         labels=['buildstream-benchmark'],
         mounts=mounts,
         privileged=True,
      )
      container_set.append(container.short_id)

      try:
         logging.debug("Waiting for container to finish")

         response = container.wait()
         returncode = response['StatusCode']
         output = container.logs().decode('unicode-escape')

         logging.debug("Output: %s", output)
         logging.debug("Returncode: %s", returncode)

         if returncode != 0:
            raise RuntimeError("Test script returned non-zero exit code.")

         measurements = read_measurements(container, test_spec.measurements_file)

         all_measurements.append(measurements)

      except Exception as e:  # pylint: disable=broad-except
         logging.error("Error during test (iteration %s): %s", i, e)
         exception = e

         return all_measurements, returncode, output, exception
      finally:
         container.stop()
         container.remove(v=True)
         container_set.remove(container.short_id)

   logging.info(
      "Test '%s' completed on version '%s'",
      test_spec.name,
      version.spec.unique_ref)

   return all_measurements, 0, "", None


# TODO: Make _debug actually do something
def run(config_files, _debug, keep_images, reuse_images, output_file):
   start_time = datetime.datetime.now()
   logging.info("BuildStream benchmark runner started at %s", start_time)

   benchmark_config = config.BenchmarkConfig()

   # Only load the default config if no config was specified. If one or more was
   # specified, load them all in order.
   if not config_files:
      benchmark_config = config.load(DEFAULT_CONFIG_FILE, benchmark_config)
   else:
      for config_filename in config_files:
         benchmark_config = config.load(config_filename, benchmark_config)

   if not benchmark_config.test_specs:
      raise RuntimeError("You must have at least one test specified in your "
                         "config file - none were found.")

   if not benchmark_config.version_specs:
      raise RuntimeError("You must have at least one Buildstream version specified "
                         "in your config file - none were found.")

   host_info = hostanalysis.get_host_info()

   docker_client = docker.from_env()

   benchmarking_git_sha = git.Repo(search_parent_directories=True).head.object.hexsha

   try:
      docker_client.ping()
   except requests.exceptions.ConnectionError as e:
      raise RuntimeError("Could not connect to Docker daemon: socket not found. "
                         "Set DOCKER_HOST in the environment if it is at a "
                         "non-standard path: {}".format(e))
   except Exception as e:
      raise RuntimeError("Could not connect to Docker daemon: {}".format(e))

   versions_dict = {}
   volumes_dict = {}
   repos_dict = {}
   with tempfile.TemporaryDirectory(prefix='temp_repo_store') as repo_staging_area:
      for version_spec in benchmark_config.version_specs:
         if version_spec.buildstream_repo not in repos_dict:
            tdir = tempfile.mkdtemp(prefix='temp_repo', dir=repo_staging_area)
            try:
               repo = git.Repo.clone_from(version_spec.buildstream_repo, tdir)
               logging.info("Checking out from %s", version_spec.buildstream_repo)
               repo.git.checkout(version_spec.buildstream_ref)
               repos_dict[version_spec.buildstream_repo] = repo
            except git.exc.GitError:  # pylint: disable=no-member
               logging.error("Unable to clone repo %s", version_spec.buildstream_repo)
         else:
            logging.info(
               "Checking out from pre cached repo %s",
               version_spec.buildstream_repo)
            repos_dict[version_spec.buildstream_repo].git.checkout(version_spec.buildstream_ref)
         version = None
         if reuse_images:
            version = reuse_prepared_version(docker_client, version_spec)
         if version is None:
            try:
               version = prepare_version(docker_client, version_spec, bs_repo=repos_dict)
            except: # pylint: disable=bare-except
               logging.error("Unable to prepare version %s skipping", version_spec)
               version = None
         versions_dict[version_spec.unique_ref] = version

      for volume_spec in benchmark_config.volume_specs:
         try:
            volume = prepare_volume(docker_client, volume_spec, versions_dict)
         except Exception as e: # pylint: disable=broad-except
            logging.error("Unable to prepare volume version %s skipping", e)
            volume = None
         volumes_dict[volume_spec.name] = volume

   results_list = []
   for test in benchmark_config.test_specs:
      result = TestResult(test)
      for version in versions_dict.values():
         try:
            result_for_version = run_benchmark(docker_client, version, test, volumes_dict)
         except: # pylint: disable=bare-except
            logging.error("Unable to to test version skipping")
            continue
         result.record_result_for_version(version, *result_for_version)
      results_list.append(result)

   if not keep_images and not reuse_images:
      logging.info("Deleting test-specific Docker images")
      for version in versions_dict.values():
         if version is not None:
            docker_client.images.remove(version.image.id)

   for cont in container_set:
      subprocess.run(["docker", "rm", str(cont)], check=False)

   end_time = datetime.datetime.now()
   logging.info("BuildStream benchmark runner finished at %s", end_time)

   logging.info("Writing results to stdout")
   vers = []
   for ver in versions_dict.values():
      if ver is not None:
         vers.append(ver)
   ress = []
   for res in results_list:
      if res is not None:
         ress.append(res)
   output = {
      'start_timestamp': start_time.timestamp(),
      'end_timestamp': end_time.timestamp(),
      'host_info': host_info,
      'benchmarking_sha': benchmarking_git_sha,
      'versions': [version.describe() for version in vers],
      'tests': [result.describe() for result in ress]
   }

   if output_file:
      logging.info("Writing to file %s", output_file)
      try:
         with open(output_file, 'w') as fp:
            json.dump(output, fp, indent=4)
      except IOError:
         logging.error("Unable to write results to file: %s", output_file)
      except TypeError:
         logging.error("Unable to generate json file")
   else:
      logging.info("Writing results to stdout")
      json.dump(output, sys.stdout, indent=4)
