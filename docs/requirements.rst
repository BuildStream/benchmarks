Requirements for BuildStream benchmarking
=========================================

This is a list of requirements for benchmarking BuildStream. Note that
a requirement here does not indicate that that requirement has been
implemented.

The requirements for benchmarking break down into two main aims:

1) Provide data and insight for people who want to improve the performance of BuildStream
2) Protect BuildStream from unexpected performance reductions.


Detailed requirements
=====================

1. Provide data and insight for people who want to imrove the performance of BuildStream

   1.1. Specify and provide scenarios for testing BuildStream:

      1.1.1. Provide basic benchmarks which measure the startup speed of BuildStream

      1.1.2. Fetch and build real-world projects such as freedesktop-sdk.

      1.1.3. Rebuild projects which have previously been fetched or previously built.

      1.1.4. Build artificially-constructed BuildStream projects which have order-of-magnitude differences in the number of components.

      1.1.5 Measure the time taken by individual bst commands: fetch, build, checkout, and shell.

   1.2 Collect statistics. For each of the scenrios in 1.1, collect:

      1.2.1 Total runtime (by wallclock)

      1.2.2 Disk/storage usage

      1.2.3 Memory usage

      1.2.4 Network bandwidth used.

   1.3 Present the results.

      1.3.1 Present the results of the benchmark in a tabular format which allows easy comparison between two different versions of buildstream.

      1.3.2 Provide the results in a documented JSON format for further analysis

      1.3.3 Provide the results in CSV format

	    Rationale: CSV is the easiest format to copy into a spreadsheet or Jupyter notebook for ad-hoc analysis.

      1.3.4 Where two versions are compared, clearly indicate significant reductions in performance.

      1.3.5 Provide graphs of performance broken down by the scale of the project

	    It should, for example, be possible to see the total time and time per element for projects involving 1, 10, 100 and 1000 elements - and higher if necessary.

Example graph, showing total build time vs number of files::

                local files: build compose.bst (secs)
  35 +-+----+------+------+------+------+------+------+------+------+----+-+
     +      +      +      +      +      +      +      +      +   ****      +
  30 +-+                                                  *******        +-+
     |                                               *****                 |
     |                                           ****                      |
  25 +-+                                    *****                        +-+
     |                                 *****                               |
  20 +-+                           ****                                  +-+
     |                        *****                                        |
     |                   *****                                             |
  15 +-+             ****                                                +-+
     |          *****                                                      |
  10 +-+    ****                                                         +-+
     |    **                                                               |
     |  **                                                                 |
   5 +**                                                                 +-+
     *      +      +      +      +      +      +      +      +      +      +
   0 +-+----+------+------+------+------+------+------+------+------+----+-+
     0     1000   2000   3000   4000   5000   6000   7000   8000   9000  10000
                                   file count


2. Protect BuildStream from unexpected performance reductions.

   2.1. Run the benchmarks automatically on each branch which is a candidate to be merged into BuildStream.

   2.2. Provide a clear indication if a branch has introduced a performance regression against the current master branch.


3. Interface requirements

   3.1. Provide simple options to run tests for a given length of time, for example 'quick' (1 minute), 1 hour or overnight testing scenarios.
