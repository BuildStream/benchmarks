#!/usr/bin/env python3

#
#  Copyright (C) 2019 Codethink Limited
#  Copyright (C) 2019 Bloomberg Finance LP
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Lachlan Mackenzie <lachlan.mackenzie@codethink.co.uk>

import json
import os
import logging
import argparse
import sys
import time
import datetime
import statistics
import csv
import collections


def main():
   output_file = 'digest.mdwn'
   error_file = 'error.mdwn'
   files = list()
   job_name = None

   parser = argparse.ArgumentParser()
   parser.add_argument(
      "-d",
      "--directory",
      help="Directory containing multiple results files (*.json), default is current directory.",
      type=str)
   parser.add_argument("-s", "--specific_results",
                       help="Path to a specific results set",
                       type=str)
   parser.add_argument("-o", "--output_file",
                       help="Output file for results digest",
                       type=str)
   parser.add_argument("-j", "--ci_job",
                       help="Ci job name",
                       type=str)

   args = parser.parse_args()

   if bool(args.directory):
      if os.path.isdir(args.directory):
         for entry in os.scandir(args.directory):
            if entry.name.endswith(".json"):
               files.append(entry.path)
      else:
         logging.error("Specified directory does not exist: %s", args.directory)
         sys.exit(1)

   if bool(args.specific_results):
      if os.path.isfile(args.specific_results):
         if args.specific_results.endswith(".json"):
            files.append(args.specific_results)
      else:
         logging.error("Specific results file does not exist: %s", args.specific_results)
         sys.exit(1)

   if bool(args.output_file):
      output_file = args.output_file

   if bool(args.ci_job):
      job_name = args.ci_job

   parse(files, output_file, error_file, job_name)


def append_error(error_subtype, error_msg, resultsdict: dict, result_file_name: str, version, name):
   logging.error(error_msg)
   error_key = {'version': version,
                'name': name,
                'type': 'error',
                'subtype': error_subtype,
                'file': result_file_name,
                'item': len(resultsdict),
                'commit_time': time.strftime("%Y%m%d-%H%M%S")}

   ice_error_key = frozenset(error_key.items())
   error = {'error': error_msg, # Error message to be printed
            'file': '{}'.format(result_file_name), # Results file name that contains error
            'version': version, # Test version that contains error
            'error_type': error_subtype, # What type of error it is
            'commit_time': time.strftime("%Y%m%d-%H%M%S")} # The time at which the error is found

   resultsdict[ice_error_key] = error


def parse(files, output_file, _error_file, ci_job=None):
   resultsdict = dict()
   for entry in files:
      try:
         with open(entry, "r") as f:
            file = f.read()
            data = json.loads(file)
            try:
               measurement_time = data["end_timestamp"]
               version_ref = {}
               version_date = {}

               for version in data["versions"]:
                  version_ref[str(version["unique_ref"])] = version["buildstream_commit"]

                  if 'buildstream_commit_date' in version:
                     if version["buildstream_commit_date"]:
                        version_date[str(version["buildstream_commit"])] = float(
                           version["buildstream_commit_date"])
                     else:
                        version_date[str(version["buildstream_commit"])] = measurement_time
                  else:
                     version_date[str(version["unique_ref"])] = measurement_time

               for test in data["tests"]:
                  name = test["name"]
                  for result in test["results"]:
                     version = result["version"]

                     # Check if measurements have been made
                     if "measurements" not in result:
                        logging.warning("Measurement corruption in: %s", f.name)
                        error_msg = "Measurement Missing"
                        append_error('missing', error_msg, resultsdict, f.name, version, name)
                        continue

                     bs_ref = None
                     bs_commit = None
                     times = []
                     rss_kbs = []
                     # Iterate measurements and add
                     for measurement in result["measurements"]:
                        times.append(measurement["total-time"])
                        rss_kbs.append(measurement["max-rss-kb"])
                        if ("bs-ref" in measurement) and ("bs-sha" in measurement):
                           if not bs_ref:
                              bs_ref = measurement["bs-ref"]
                              bs_commit = measurement["bs-sha"]
                           else:
                              if measurement["bs-ref"] != bs_ref:
                                 error_msg = "Buildstream reference changed from {} to {}".format(
                                    bs_ref, result["bs-ref"])
                                 append_error('ref', error_msg, resultsdict, f.name, version, name)
                                 bs_ref = result["bs-ref"]
                              if measurement["bs-sha"] != bs_commit:
                                 error_msg = "Buildstream commit changed from {} to {}".format(
                                    bs_commit, measurement["bs-sha"])
                                 append_error('commit',
                                              error_msg,
                                              resultsdict,
                                              f.name,
                                              version,
                                              name)
                                 bs_commit = measurement["bs-sha"]
                        if not bs_commit:
                           if "bs-sha" in measurement:
                              bs_commit = measurement["bs-sha"]
                           elif "bs-ref" in result:
                              bs_commit = measurement["bs-ref"]

                     if str(version) in version_ref:
                        commit = version_ref[str(version)]
                     else:
                        commit = version_ref[str(commit)]

                     if str(commit) in version_date:
                        commit_time = version_date[str(commit)]
                     else:
                        commit_time = version_date[str(version)]

                     # Calculate averages
                     average_time = statistics.mean(times)
                     average_max_rss_kb = statistics.mean(rss_kbs)

                     # Standard deviations
                     times_sd = statistics.stdev(times)
                     rss_kbs_sd = statistics.stdev(rss_kbs)

                     # Create a key based on version and name
                     key = {}
                     key['version'] = version
                     key['name'] = name
                     key['type'] = 'result'
                     key['subtype'] = 'none'
                     key['file'] = f.name
                     ice_key = frozenset(key.items())

                     # Create a value for the entry
                     value = {
                        # Unique reference created from branch/sha reference plus repository
                        'version': version,
                        # commit reference or sha requested for benchmarking
                        'commit_ref': commit,
                        # Time at which the measurement was takem
                        'measurement_time': datetime.datetime.fromtimestamp(measurement_time),
                        # Average (mean) time taken to carry out test
                        'test_mean_time': average_time,
                        # Average (mean) maximum RSS for test
                        'mean_max_rss': average_max_rss_kb,
                        # The timestamp of the head commit being tested
                        'commit_time': datetime.datetime.fromtimestamp(commit_time),
                        # Standard deviation or average time taken
                        'test_mean_time_sd': times_sd,
                        # Standard deviation of average mean RSS
                        'test_rss_kbs_sd': rss_kbs_sd,
                        # SHA of head commit being tested
                        'commit_sha': bs_commit,
                        # File name of test data being processed
                        'file': f.name}

                     # Add the value to the accumulated values for a given key
                     if ice_key in resultsdict:
                        error_msg = "Data has been duplicated in results for: {}".format(version)
                        logging.error(error_msg)
                        append_error('duplication', error_msg, resultsdict, f.name, version, name)
                     else:
                        resultsdict[ice_key] = value

                     logging.debug("%s %s %s %s %s %s %s %s %s", version,
                                   name, commit, measurement_time, average_time,
                                   times_sd, average_max_rss_kb, rss_kbs_sd, bs_commit)

            except ValueError as error:
               logging.error("Error during parse of %s: %s", file, error)
      except ValueError as error:
         logging.error("Failure to load %s as json file: %s", file, error)


   # Define method for sorting results keys
   def get_sort_key(entry: (str, str)) -> str:
      # The key in the results dictionary is a frozenset dictionary containing a number of key
      # fields that can differentiate between different test result configurations. Further if a
      # duplication occurs then an error can be flagged (only one unique set of discernable results
      # should be generated. Strings are retained for readability.

      # To reference the frozenset "key" dictionary it is translated back into a standard
      # dictionary the "key" is naturally the first entry in the set that is passed in, i.e.
      # entry[0]. A typical "key" entry is as follows frozenset({('type', 'result'),
      # ('name', 'Startup time'), ('subtype', 'none'), ('file', 'results.json'),
      # ('version', 'master.https...gitlab.com.buildstream.buildstream')}
      # There is some overlap between "key" and "results" fields, this is retained for simplicity.
      entry = dict(entry[0])
      # From this "key" dictionary the name is extracted and used to sort the overall list of
      # results
      return entry['name']


   with open(output_file, 'w') as results_file:
      # Add set of CSV headers that will be used
      headers = ['Job ID', 'Result Date', 'Error Type', 'Error Message', 'Test Version',
                 'Test Name', 'Time', 'Average Time to Complete Test (s)',
                 'Time Standard Deviation', 'Average Resident Set Size (kb)',
                 'RSS Standard Deviation', 'SHA', 'Results File']

      # Map result/error dictionary fields to CSV headers
      header_indices = {'measurement_time': "Time",
                        'test_mean_time': "Average Time to Complete Test (s)",
                        'mean_max_rss': "Average Resident Set Size (kb)",
                        'commit_time': "Result Date",
                        'test_mean_time_sd': "Time Standard Deviation",
                        'test_rss_kbs_sd': "RSS Standard Deviation",
                        'commit_sha': "SHA",
                        'error': 'Error Message',
                        'file': 'Results File',
                        'version': 'Test Version',
                        'error_type': 'Error Type'}

      csv_writer = csv.DictWriter(results_file, delimiter=',', fieldnames=headers,
                                  quotechar='|', restval='', quoting=csv.QUOTE_MINIMAL)
      csv_writer.writeheader()

      sort_dict = collections.OrderedDict(sorted(resultsdict.items(),
                                                 key=get_sort_key, reverse=True))

      for key, value in sort_dict.items():
         dict_k = dict(key)
         # Translate results/errors to have CSV equivalent fields
         translation = {header_indices[index]: datum for index, datum in value.items()
                        if index in header_indices}
         # Set the CI job ID directly (note this could be 'None' for local workflow
         translation['Job ID'] = str(ci_job)
         # Set the test name directly
         translation['Test Name'] = dict_k['name']
         # Write the translated data
         csv_writer.writerow(translation)


if __name__ == "__main__":
   main()
