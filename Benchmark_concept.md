Concept For Benchmarking Usage
==============================

Please see README.rst for further information about running Benchmarking
manually and broader information about the tests.

Usage Concept
=============

Benchmarking is required to ascertain how Buildstream performance changes with
time. Changes might be due to modifications to the Benchmarking code base (e.g.
performance improvements or the introduction of necessary overhead) or a change
to the underlying environment and/or dependencies that Buildstream has to work
with in a typical environment (e.g. Kernel changes on host machine, security
updates etc).

It is probable that prior to incorporating new functionality or changes into
Buildstream that developers need to ascertain what the impact of the prospective
modification/addition will be, therefore Benchmarking should allow users to
check the performance of prospective branches prior to merge.

Triggering
==========

The original concept was that Benchmarking would be carried out each time a
merge was carried out on Buildstream master, but this is not possible so the
current concept is that it will be done either on demand or via a cron job. The
cron job would trigger the Benchmarking pipeline process and all merge commits
in the past day would be stepped through and Benchmarking would process each one
in turn. If there are no merges in the past day the last commit of Buildstream
will be retested so that underlying host changes can be evaluated on any given
day. The triggering of Benchmarking builds so that this "following" of
Buildstream changes are carried out is via the "FOLLOW_BUILDSTREAM" tag in
the build trigger call.

The approach of using a gitlab.ci runner has been retained given the possibility
that there will still be triggering of benchmarking from prospective branches
in buildstream and that it is preferable that this is done automatically without
handcrafting builds in benchmarking directly. The use of gitlab runners also
provides an easy mechanism for downloading results via build artifact.zip files.

Current triggering runs on a dedicated Benchmarking machine via a cron job that
is run at midnight. It is preferable not to run parallel builds (i.e. triggering
a manual build at the same time as the automatic build is running) and as such
any manual should be triggered with full understanding of the automatic
schedule and how long any tests would take. It is hoped that a scheduling
interlock could be put in place to limit and stack build requests in the future.

Benchmark Running
=================

The default benchmarks are currently used for checking buildstream changes, but
it is probable that the test suite would have to be expanded at a later date to
cover other types of test (this is currently out of scope though). It is
probable that this would need to be selectable given the dynamics of a
particular change in a branch or some other factor (e.g. tests taking too long
to be run every day and being scheduled every month only).

When the Benchmarking job is triggered with the "FOLLOW_BUILDSTREAM" tag set,
the last set of commits since the last trigger are determined via a "Run
Token" that is written at the end of each session. The run token makes it
clear what the last complete Benchmarking run was and contains:

- Which Benchmarking branch was executed
- What Benchmarking commit was the HEAD at the time
- Which Buildstream branch was tested
- What the Buildstream commit HEAD was at the time
- The last results file that was generated.
- The time/date of completion.

The current Buildstream repository head commit is got at the start of the
process and is used as a reference to get all the commits between the last time
Benchmarking following was run (the Benchmarking commit SHA in the token file)
and the current head.

The individual SHAs retrieved are used to generate a Benchmarking configuration
file that extends the set of tests for specific versions (addon-version.benchmark
extension). The benchmark tests are run sequentially as separate jobs in the
gitlab pipeline, reduces the effect of performance being modified due to
resource contention (similarly it is the intent to make the gitlab runner only
run one pipeline at a time).

When a benchmark test completes the results are used (together with historical
results) to generate graphs and html pages which are added to the job
artifact.zip (currently hosted at
https://buildstream.gitlab.io/buildstream-benchmarking)

When all the benchmark tests complete all graphical results are correlated as
part of the pipeline_cache within the deploy stage of the Buildstream pipeline
chain. All raw json results (that pertain to the "master" set) are retained as
part of the results_cache (see Run Results below).

Run Results
===========

The results from each Benchmarking run are reflected as artifacts in the gitlab
pipeline. This pipeline is run on a dedicated machine which provides a reference
environment in which comparable tests can be carried out.

Each artifact.zip contains 3 directories:

results_out    - these are the results for this specific job in the pipeline
results_cache  - these are the longitudinal results that are retained
pipeline_cache - these are cached results for all the jobs in a particular
                 pipeline run and are cleared out at the beginning of each run.

To create a sustainable statistical data set to work with it is envisaged that
the results will be retained in such a way that tenable longitudinal data sets
can be made available for analysis and that this might be beyond the range of
the retention timespan that gitlab provides in its build artefact handling.

Currently gitlab runner caching has been employed to retain data, but it is
envisaged that this will be superseded by a higher integrity solution. The use
of external data storage will not remove output artefacts from being generated
by the runner.

It is envisaged that only "de-facto" results using master Benchmarking testing
and master Buildstream will be retained, these results will act as the
comparables against which prospective Buildstream and Benchmarking branches will
be compared. Other results (e.g. a prospective Buildstream branch) will be
presented as part of Benchmarking artefacts for a Benchmarking run that is
specified to test that branch, but this data would not be retained for
permanent comparison as part of the "master" data set.

To provide immediate feedback the "standard" Benchmarking results of the current
benchmark build are plotted with the historical set and added to the
artifact.zip generated by the Buildstream pipeline as part of a navigable html/
png set of files.

Display of Headline Results
===========================

Benchmark results that pertain to "master" data set runs will be displayed in a
[gitlab.io instance](https://buildstream.gitlab.io/buildstream-benchmarking). The
results are delineated by test base version and test name (i.e. there is one page
for each combination of test base version and name). The gitlab.io update is
triggered by a request from Benchmarking in the final stage of the build (only
if the master branches are being built and the 'PUBLISH_RESULT' tag is set
in the build trigger call). The gitlab.io instance gets the latest
artifact.zip from the Benchmarking pipeline, unpacks it so the pages can be
displayed and then creates a master navigable index as a central entry point.

Example Benchmarking Build Call
===============================

Below is an example of the form of script that is needed to trigger Benchmarking
builds.

#!/bin/bash

curl --request POST \
  --form token=<CI TOKEN> \
  --form ref='master' \
  --form "variables[PUBLISH_RESULT]=TRUE" \
  --form "variables[FOLLOW_BUILDSTREAM]=TRUE" \
  https://gitlab.com/api/v4/projects/<Project Ref>/trigger/pipeline

The <CI Token> and <Project Ref> can be found under the settings page for the
Benchmarking project CI/CD.
